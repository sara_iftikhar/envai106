"""
==================
utility functions
==================
"""

import os
import time
from multiprocessing import cpu_count

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from easy_mpl.utils import make_cols_from_cmap
from easy_mpl import pie
from easy_mpl.utils import despine_axes
from easy_mpl.utils import AddMarginalPlots

import seaborn as sns

from ai4water.utils.utils import get_version_info
from sklearn.preprocessing import OneHotEncoder

# %%
SAVE = False

LABEL_MAP = {}

# def hardware_info()->dict:
#     mem_bytes = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')  # e.g. 4015976448
#     mem_gib = mem_bytes / (1024. ** 3)  # e.g. 3.74
#     return dict(
#         tot_cpus=cpu_count(),
#         avail_cpus = len(os.sched_getaffinity(0)),
#         mem_gib=mem_gib,
#     )

def print_version_info(
        include_run_time:bool=True,
        include_hardware_info:bool = True
):
    info = version_info()
    if include_run_time:

        info['Script Executed on: '] = time.asctime()

    # if include_hardware_info:
    #     info.update(hardware_info())

    for k,v in info.items():
        print(k, v)
    return

# %%

def version_info()->dict:
    """
    returns the version info of packages being used
    """

    info = get_version_info()

    return info

# %%

def _ohe_column(df:pd.DataFrame, col_name:str) -> tuple:

    assert isinstance(col_name, str)

    encoder = OneHotEncoder(sparse=False)
    ohe_cat = encoder.fit_transform(df[col_name].values.reshape(-1,1))
    cols_added = [f"{col_name}_{i}" for i in range(ohe_cat.shape[-1])]

    df[cols_added] = ohe_cat

    df.pop(col_name)

    return df, cols_added, encoder

def prepare_data(fpath):

    #fpath = os.path.join("../data/data.xlsx")
    df = pd.read_excel(fpath)

    data, _, ct_encoder = _ohe_column(df, 'Catalyst type')
    data, _, anion_encoder = _ohe_column(data, 'Anions')

    #moving target to last
    target = data.pop('Efficiency (%)')
    data['Efficiency (%)'] = target

    return data, ct_encoder, anion_encoder


# %%

def distribution_plot(ax, data, scatter_fc='#045568',
                      box_facecolor='#e6e6e6',
                      width=0.8,
                      add_hist=True,
                      add_ridge=True):

    sns.boxplot(orient='h', data=data, saturation=1, showfliers=False,
                width=width, boxprops={'zorder': 3, 'facecolor': box_facecolor}, ax=ax,
                )
    old_len_collections = len(ax.collections)

    for dots in ax.collections[old_len_collections:]:
        dots.set_offsets(dots.get_offsets() + np.array([0, 0.12]))

    ax = sns.stripplot(orient='h', x=data,
                       edgecolor="gray",
                       #linewidth=0.1,
                       alpha=0.5,
                       c=scatter_fc,
                       size=1.5,
                        ax=ax,
                       jitter=0.2)

    despine_axes(ax, keep=['bottom', 'left', 'right'])

    if add_hist or add_ridge:
        aa = AddMarginalPlots(ax=ax,
                              hist=add_hist,
                              ridge=add_ridge,
                              hist_kws=dict(bins=20, color=box_facecolor),
                              fill_kws=dict(color=box_facecolor),
                              ridge_line_kws=dict(color=scatter_fc))
        aa.divider = make_axes_locatable(ax)
        axHistx = aa.add_ax_marg_x(data.values.reshape(-1,), hist_kws=aa.HIST_KWS[0], ax=None)
        plt.setp(axHistx.get_xticklabels(),visible=False)

    return ax

# %%

def merge_uniques(
        series:pd.Series,
        n_to_keep:int=5,
        replace_with="Rest"
):
    counts = series.value_counts()

    values = []
    for idx, (value, count) in enumerate(counts.items()):
        if idx >= n_to_keep:
            values.append(value)

    series = series.replace(values, replace_with)
    return series

# %%

def pie_from_series(
        data:pd.Series,
        cmap="tab20",
        label_percent:bool = True,
        n_to_merge:int = None,
        leg_pos=None,
        show=True,
        fontsize=14
):

    d:pd.Series = data.value_counts()
    labels = d.index.tolist()
    vals = d.values
    colors = make_cols_from_cmap(cm=cmap, num_cols=len(vals))
    percent = 100. * vals / vals.sum()

    outs = pie(fractions=percent, autopct=None,
               colors=colors, show=False)
    patches, texts = outs

    if label_percent:
        labels = ['{0}: {1:1.2f} %'.format(i, j) for i, j in zip(labels, percent)]
    else:
        labels = ['{0} (n={1:4})'.format(i, j) for i, j in zip(labels, vals)]

    patches, labels, dummy = zip(*sorted(zip(patches, labels, vals),
                                         key=lambda x: x[2],
                                         reverse=True))

    plt.legend(patches, labels, bbox_to_anchor=leg_pos or (1.1, 1.),
               fontsize=fontsize)

    if show:
        plt.tight_layout()
        plt.show()
    return
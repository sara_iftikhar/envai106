"""
==============
chord diagram
==============
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mne.viz import circular_layout
from mne_connectivity.viz import plot_connectivity_circle

from easy_mpl import imshow

from utils import prepare_data


# %%
fpath = os.path.join("../data/data.xlsx")

df, _, _ = prepare_data(fpath)

# %%

corr = df.corr(method="pearson")

# %%

imshow(corr, colorbar=True, show=False)
plt.tight_layout()
plt.show()

# %%

df = df.fillna(0.0)

# %%

node_angles = circular_layout(corr.columns.tolist(), corr.columns.tolist(),
                              start_pos=90, group_boundaries=[0, len(corr.columns.tolist()) // 2])

print(node_angles.shape)

# %%

fig, ax = plt.subplots(figsize=(16, 16),
                       facecolor="#EFE9E6",
                       subplot_kw=dict(polar=True))
fig, axes = plot_connectivity_circle(
    corr.values,
    node_names = corr.columns.tolist(),
    node_angles=node_angles,
    fontsize_names =14,
    fontsize_colorbar =14,
    facecolor ="#EFE9E6",
    textcolor='black',
    #n_lines = 14,
    node_edgecolor="white",
    colormap="Spectral",
    colorbar_size=0.5,
    colorbar_pos=(-0.5, 0.5),
    ax=ax)

fig.savefig(f"figures/chord_large", dpi=600, bbox_inches="tight")
fig.tight_layout()

# %%

df_org = pd.read_excel(fpath)
print(df_org.shape)

df_org = df_org.drop(columns=['Catalyst type', 'Anions'])

corr = df_org.corr(method="pearson")

imshow(corr, colorbar=True, show=False)
plt.tight_layout()
plt.show()

# %%

df_org = df_org.fillna(0.0)

# %%

node_angles = circular_layout(corr.columns.tolist(), corr.columns.tolist(),
                              start_pos=90, group_boundaries=[0, len(corr.columns.tolist()) // 2])

print(node_angles.shape)

# %%

fig, ax = plt.subplots(figsize=(16, 16),
                       facecolor="#EFE9E6",
                       subplot_kw=dict(polar=True))
fig, axes = plot_connectivity_circle(
    corr.values,
    node_names = corr.columns.tolist(),
    node_angles=node_angles,
    fontsize_names =14,
    fontsize_colorbar =14,
    facecolor ="#EFE9E6",
    textcolor='black',
    #n_lines = 14,
    node_edgecolor="white",
    colormap="Spectral",
    colorbar_size=0.5,
    colorbar_pos=(-0.5, 0.5),
    ax=ax)

fig.savefig(f"figures/chord_large_org", dpi=600, bbox_inches="tight")
fig.tight_layout()






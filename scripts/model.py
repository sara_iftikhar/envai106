
"""
======
model
======
"""

import os
import shap
import pandas as pd
import matplotlib.pyplot as plt

from ai4water import Model
from ai4water.postprocessing import ProcessPredictions
from ai4water.utils.utils import TrainTestSplit
from SeqMetrics import RegressionMetrics

from utils import prepare_data


# %%
fpath = os.path.join("../data/data.xlsx")

# %%
# original data

df = pd.read_excel(fpath)
print(df.shape)

# %%

print(df.columns)

# %%

print(df.head())
# %%

print(df.describe())

# %%
# preprocessed data (CategorOne-Hot encoded)

data, _, _ = prepare_data(fpath)

# %%

print(data.shape)

# %%

print(data.columns)

# %%

print(data.head())

# %%

TrainX, TestX, TrainY, TestY = (TrainTestSplit(seed=313).
                                split_by_random(data.iloc[:,0:-1], data.iloc[:,-1]))

# %%

print(TrainX.shape)

# %%

print(TrainY.shape)

# %%

print(TestX.shape)

# %%

print(TestY.shape)

# %%

model = Model(model='XGBRegressor',
              cross_validator={"KFold": {'n_splits': 50}}
              )

# %%

model.fit(TrainX, TrainY)

# %%

train_p = model.predict(TrainX)

# %%

tr_metrics = RegressionMetrics(TrainY.values, train_p)

print(f"training r2_score: {tr_metrics.r2_score()}")

# %%

processor = ProcessPredictions('regression', forecast_len=1,
                               plots=['prediction', 'regression', 'residual'])

processor(TrainY.values, train_p)

# %%

test_p = model.predict(TestX)

# %%

test_metrics = RegressionMetrics(TestY.values, test_p)

print(f"test r2_score: {test_metrics.r2_score()}")

# %%

processor = ProcessPredictions('regression', forecast_len=1,
                               plots=['prediction', 'regression', 'residual'])

processor(TestY.values, test_p)

# %%

# SHAP
explainer = shap.Explainer(model._model, TrainX)

shap_values = explainer(TrainX)

shap.summary_plot(shap_values, TrainX.values,
                  plot_type="bar",
                  feature_names=data.columns[0:-1], show=False)

plt.savefig('figures/shap_bar.png')

# %%

shap.summary_plot(shap_values, TrainX.values,
                  feature_names=data.columns[0:-1], show=False)

plt.savefig('figures/shap.png')


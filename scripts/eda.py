"""
=========================
Exploratory Data Analysis
=========================
"""
import os
import pandas as pd

import matplotlib.pyplot as plt

from easy_mpl.utils import create_subplots

from utils import SAVE
from utils import LABEL_MAP
from utils import distribution_plot
from utils import print_version_info
from utils import pie_from_series
from utils import merge_uniques

# %%

# Print the version info of the packages being used
print_version_info()

# %%
fpath = os.path.join("../data/data.xlsx")
df = pd.read_excel(fpath)

# %%

# Display the first 5 rows of the dataset
df.head()

# %%
# Display the last 5 rows of the dataset
df.tail()

# %%
# Display the shape of the dataset
df.shape

# %%
# Display the columns of the dataset
df.columns

# %%
# Display the info of the dataset
df.info()

# %%
# Display the summary statistics of the dataset
df.describe()

# %%
# Display the missing values of the dataset
df.isnull().sum()

# %%
# Display the duplicated rows of the dataset
df.duplicated().sum()

# %%
# categorical columns
cat_columns = ['Catalyst type', 'Anions']

# %%
# Display the unique values of the categorical columns
for col in cat_columns:
    print(f"{col}: {df[col].unique()}")

# %%
# Display the value counts of the categorical columns
for col in cat_columns:
    print(f"{col}: {df[col].value_counts()}")


# %%
# numerical columns
num_columns = ['Surface area', 'Pore volume', 'BandGap (eV)', 'Au',
       'Bi', 'Fe', 'O', 'Catalyst loading (g/L)', 'Light intensity (W)',
       'time (min)', 'solution pH', 'Ci (mg/L)', 'Cf (mg/L)',
       'Efficiency (%)']

# %%
# Display the distribution of the numerical columns
for col in num_columns:
    print(f"{col}: {df[col].describe()}")


data_num = df[num_columns].copy()
# %%

fig, axes = create_subplots(data_num.shape[1], figsize=(9, 8))
for ax, col in zip(axes.flat, data_num.columns):
    distribution_plot(ax=ax, data=data_num[col])
    ax.set_xlabel(xlabel=LABEL_MAP.get(col, col), weight='bold', fontsize=14)
    ax.set_yticklabels('')
plt.tight_layout()
if SAVE:
    plt.savefig("../manuscript/figures/distribution_num.png", dpi=600,
                bbox_inches="tight")
plt.show()

# %%

merged_series = merge_uniques(df['Catalyst type'], 2)
pie_from_series(merged_series, cmap="coolwarm",  show=False, leg_pos=(0.85, 0.7))